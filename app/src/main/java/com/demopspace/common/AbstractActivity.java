package com.demopspace.common;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;

import com.demopspace.R;

import butterknife.ButterKnife;

/**
 * Created by Spiros I. Oikonomakis on 1/29/15.
 */
public abstract class AbstractActivity extends ActionBarActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);
    }

    @Override
    public void setContentView(int layoutResID)
    {
        super.setContentView(layoutResID);

        ButterKnife.inject(this);
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        BusProvider.getInstance().register(this);
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        BusProvider.getInstance().unregister(this);
    }

    protected void createFragment(Fragment targetFragment)
    {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, targetFragment, "fragment").commit();
    }
}
