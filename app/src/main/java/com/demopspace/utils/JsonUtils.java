package com.demopspace.utils;

import org.apache.http.HttpEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by soikonomakis on 3/21/14.
 */
public class JsonUtils {

    /**
     * Returns from httpEntity the contect
     * to jsonString
     * @param entity
     * @return
     */
    public static synchronized String extractJsonAsString(HttpEntity entity) {

        InputStream is = null;

        try {
            is = entity.getContent();
        } catch (IllegalStateException e) {

            return null;

        } catch (IOException e) {

            return null;

        } catch (Exception e) {

            return null;
        }

        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder  sb     = new StringBuilder();
        String         line   = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append((line + "\n"));
            }
        } catch (IOException e) {
            return null;
        } catch (Exception e) {
            return null;
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                return null;
            }
        }
        return sb.toString();
    }
}
