package com.demopspace.net.managers;

import android.os.AsyncTask;

import com.demopspace.common.BusProvider;
import com.demopspace.net.events.FoursquareExploreEvent;
import com.demopspace.net.responses.foursquare.explore.Explore;
import com.demopspace.utils.Constants;
import com.demopspace.utils.JsonUtils;
import com.demopspace.utils.StringUtils;
import com.google.gson.Gson;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Map;

/**
 * Created by Spiros I. Oikonomakis on 1/29/15.
 */
public class FoursquareExploreTask extends AsyncTask<Map<String,String>, Void, String> {

    @Override
    protected String doInBackground(Map<String, String>... params) {

        String jsonString = null;
        Map<String, String> fields = params[0];

        String url = Constants.FOURSQUARE_API_URL+Constants.FOURSQ_EXPLORE+"?";

        for (String key : fields.keySet()) {
            url = StringUtils.joinAnd("&", "=", url, key, fields.get(key));
        }

        try {
            HttpGet httpGet = new HttpGet(url);
            HttpClient client = new DefaultHttpClient();
            HttpResponse response = client.execute(httpGet);
            jsonString = JsonUtils.extractJsonAsString(response.getEntity());
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jsonString;
    }

    @Override
    protected void onPostExecute(String jsonString) {
        super.onPostExecute(jsonString);

        if (jsonString != null) {
            Explore explore = new Gson().fromJson(jsonString, Explore.class);
            BusProvider.getInstance().post(new FoursquareExploreEvent(explore));
        }

    }
}
