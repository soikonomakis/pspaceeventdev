package com.demopspace.net.events;

import com.demopspace.net.responses.foursquare.explore.Explore;

import java.io.Serializable;

/**
 * Created by Spiros I. Oikonomakis on 1/29/15.
 */
public class FoursquareExploreEvent implements Serializable {

    private Explore response;

    public FoursquareExploreEvent(Explore response) {
        this.response = response;
    }

    public Explore getResponse() {
        return response;
    }

    public void setResponse(Explore response) {
        this.response = response;
    }
}
