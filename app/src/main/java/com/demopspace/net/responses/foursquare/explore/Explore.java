package com.demopspace.net.responses.foursquare.explore;

import com.demopspace.net.responses.foursquare.explore.common.Response;

import java.io.Serializable;

/**
 * Created by Spiros I. Oikonomakis on 11/16/14.
 */
public class Explore implements Serializable {

    public Response response;

    @Override
    public String toString() {
        return "Explore [response=" + response + "]";
    }
}
