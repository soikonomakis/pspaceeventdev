package com.demopspace.ui.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.demopspace.R;
import com.demopspace.common.BaseFragment;
import com.demopspace.net.events.FoursquareExploreEvent;
import com.demopspace.net.managers.FoursquareExploreTask;
import com.demopspace.net.responses.foursquare.explore.common.Item;
import com.demopspace.ui.ItemActivity;
import com.demopspace.ui.adapters.PlacesAdapter;
import com.demopspace.utils.Constants;
import com.squareup.otto.Subscribe;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class HomeFragment extends BaseFragment implements AdapterView.OnItemClickListener {

    @InjectView(R.id.txtWelcomMsg)
    TextView txtWelcomeMsg;

    @InjectView(R.id.lvPlaces)
    ListView lvPlaces;

    private PlacesAdapter mPlacesAdapter;
    private final SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMdd");

    public static HomeFragment newInstance(HashMap<String, Class<? extends Serializable>> params)
    {
        HomeFragment fragment = new HomeFragment();
        if (params != null && params.size() > 0) {

            Bundle args = new Bundle();
            for (String key : params.keySet()) {
                args.putSerializable(key, params.get(key));
            }
            fragment.setArguments(args);
        }
        return fragment;
    }

    public HomeFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View parentView = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.inject(this, parentView);

        txtWelcomeMsg.setText(getString(R.string.txt_welcome_msg));

        Map<String, String> fields = new HashMap<String, String>();
        fields.put("ll", "38.255893,21.745492");
        fields.put("client_id", Constants.FOURSQUARE_API_CLIENT_ID);
        fields.put("client_secret", Constants.FOURSQUARE_API_CLIENT_SECRET);
        fields.put("sortByDistance", "1");
        fields.put("limit", "15");
        fields.put("v", dateFormatter.format(new Date()));

        new FoursquareExploreTask().execute(fields);

        return parentView;
    }


    @Subscribe
    public void onFoursquareExploreEvent(FoursquareExploreEvent event)
    {
        if (event.getResponse() != null) {
            if (event.getResponse().response != null &&  event.getResponse().response.groups != null) {
                mPlacesAdapter = new PlacesAdapter(getActivity(), event.getResponse().response.groups.get(0).items);
                lvPlaces.setAdapter(mPlacesAdapter);
                lvPlaces.setOnItemClickListener(this);
            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Item item = (Item) mPlacesAdapter.getItem(position);
        if (item != null) {
            Intent intent = new Intent(getActivity(), ItemActivity.class);
            intent.putExtra(ItemActivity.KEY_ITEM, item);
            startActivity(intent);
        }
    }
}
