package com.demopspace.ui;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.demopspace.R;
import com.demopspace.common.AbstractActivity;
import com.demopspace.net.responses.foursquare.explore.common.Item;
import com.demopspace.ui.fragments.ItemViewFragment;

import java.util.HashMap;

public class ItemActivity extends AbstractActivity {

    public static final String KEY_ITEM = "KEY_ITEM";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle  = getIntent().getExtras();

        if (bundle.containsKey(KEY_ITEM)) {

            Item item = (Item)bundle.get(KEY_ITEM);
            HashMap<String, Object> data = new HashMap<String, Object>();
            data.put(KEY_ITEM, item);
            createFragment(ItemViewFragment.newInstance(data));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
