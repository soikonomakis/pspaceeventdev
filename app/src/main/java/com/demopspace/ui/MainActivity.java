package com.demopspace.ui;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.demopspace.R;
import com.demopspace.common.AbstractActivity;
import com.demopspace.ui.fragments.HomeFragment;

public class MainActivity extends AbstractActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        createFragment(HomeFragment.newInstance(null));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
